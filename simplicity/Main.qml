import QtQuick 2.0
import SddmComponents 2.0
import "SimpleControls" as Simple

Rectangle {
    
    width: 640
    height: 480
    
    LayoutMirroring.enabled: Qt.locale().textDirection == Qt.RightToLeft
    LayoutMirroring.childrenInherit: true

    TextConstants { id: textConstants }
    
    Connections {
        target: sddm
        onLoginSucceeded: {}
        onLoginFailed: {
            pw_entry.text = ""
            pw_entry.focus = true
            errorMessage.color = "red"
            errorMessage.text = textConstants.loginFailed
        }
    }
    
    Background {
        anchors.fill: parent
        source: config.background
        fillMode: Image.PreserveAspectCrop
        onStatusChanged: {
            if (status == Image.Error && source != config.defaultBackground) {
                source = config.defaultBackground
            }
        }
    }
    
    Rectangle {
        anchors.fill: parent
        color: "transparent"
        
        Rectangle {
            width: 400
            height: 250
            color: "transparent"
            anchors.centerIn: parent
            
            Image {
                id: user_icon

                // from https://stackoverflow.com/questions/6090740/image-rounded-corners-in-qml 
		            property bool rounded: true
    		        property bool adapt: true

    		        layer.enabled: rounded
    		        layer.effect: ShaderEffect {
        		      property real adjustX: user_icon.adapt ? Math.max(width / height, 1) : 1
        		      property real adjustY: user_icon.adapt ? Math.max(1 / (width / height), 1) : 1

        		      fragmentShader: "
        			      #ifdef GL_ES
            				  precision lowp float;
        			      #endif // GL_ES
        			      varying highp vec2 qt_TexCoord0;
        			      uniform highp float qt_Opacity;
        			      uniform lowp sampler2D source;
        			      uniform lowp float adjustX;
        			      uniform lowp float adjustY;

        			      void main(void) {
            				  lowp float x, y;
            				  x = (qt_TexCoord0.x - 0.5) * adjustX;
            				  y = (qt_TexCoord0.y - 0.5) * adjustY;
            				  float delta = adjustX != 1.0 ? fwidth(y) / 2.0 : fwidth(x) / 2.0;
            				  gl_FragColor = texture2D(source, qt_TexCoord0).rgba
                			* step(x * x + y * y, 0.25)
                			* smoothstep((x * x + y * y) , 0.25 + delta, 0.25)
                			* qt_Opacity;
        			      }"
    		          }

                anchors.horizontalCenter: parent.horizontalCenter
                width: 128; height: 128 
                source: user_entry.currentIcon
                smooth: true
                fillMode: Image.PreserveAspectFit
            }
            
            Simple.SimpleUserComboBox {
                id: user_entry
                anchors.horizontalCenter: user_icon.horizontalCenter
                anchors.topMargin: 10
                anchors.top: user_icon.bottom
                width: 250
                color: Qt.rgba(0, 0, 0, 0.2)
                dropDownColor: Qt.rgba(0, 0, 0, 0.2)
                borderColor: "transparent"
                textColor: "white"
                arrowIcon: "images/arrow-down.png"
                arrowColor: "transparent"
                model: userModel
                index: userModel.lastIndex

                font.pixelSize: 16
                KeyNavigation.backtab: session; KeyNavigation.tab: pw_entry
            }
            
            PasswordBox {
                id: pw_entry
                width: 250
                anchors.horizontalCenter: user_entry.horizontalCenter
                anchors.top: user_entry.bottom
                anchors.topMargin: 10
                font.pixelSize: 16
                color: Qt.rgba(0, 0, 0, 0.2)
                borderColor: "transparent"
                focusColor: Qt.rgba(0, 0, 0, 0.25)
                hoverColor: Qt.rgba(0, 0, 0, 0.2)
                textColor: "white"
                focus: true
                KeyNavigation.backtab: user_entry; KeyNavigation.tab: loginButton

                Keys.onPressed: {
                    if (event.key === Qt.Key_Return || event.key === Qt.Key_Enter) {
                        sddm.login(user_entry.currentText, pw_entry.text, session.index)
                        event.accepted = true
                    }
                }
            }
            Button {
                id: loginButton
                text: textConstants.login
                anchors.horizontalCenter: pw_entry.horizontalCenter
                anchors.top: pw_entry.bottom
                anchors.topMargin: 10
                width: 150
                color: Qt.rgba(0, 0, 0, 0.2)
                activeColor: Qt.rgba(0, 0, 0, 0.2)
                pressedColor: Qt.rgba(0, 0, 0, 0.25)
                font.pixelSize: 15
                font.bold: false
                onClicked: sddm.login(user_entry.currentText, pw_entry.text, session.index)
                KeyNavigation.backtab: pw_entry; KeyNavigation.tab: restart
            }
            
            Text {
                id: errorMessage
                anchors.horizontalCenter: loginButton.horizontalCenter
                anchors.top: loginButton.bottom
                anchors.topMargin: 10
                text: ""
                font.pixelSize: 15
                font.bold: true
                font.capitalization: Font.AllUppercase
            }  
        }
        
    }
    
    Rectangle {
        width: parent.width - 10
        height: 40
        color: "transparent"
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        
        Button {
            id: restart
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: shutdown.left
            anchors.rightMargin: 10
            text: textConstants.reboot
            color: Qt.rgba(0, 0, 0, 0.2)
            pressedColor: Qt.rgba(0, 0, 0, 0.25)
            activeColor: Qt.rgba(0, 0, 0, 0.2)
            font.pixelSize: 15
            font.bold: false
            onClicked: sddm.reboot()
            KeyNavigation.backtab: loginButton; KeyNavigation.tab: shutdown
        }
        
        Button {
            id: shutdown
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            text: textConstants.shutdown
            color: Qt.rgba(0, 0, 0, 0.2)
            pressedColor: Qt.rgba(0, 0, 0, 0.25)
            activeColor: Qt.rgba(0, 0, 0, 0.2)
            font.pixelSize: 15
            font.bold: false
            onClicked: sddm.powerOff()
            KeyNavigation.backtab: restart; KeyNavigation.tab: session
        }
        
    }
    
    Rectangle {
        id: dateClock
        width: parent.width - 10
        height: 40
        color: "transparent"
        anchors.top: parent.top;
        anchors.horizontalCenter: parent.horizontalCenter
        
        Simple.SimpleComboBox {
            id: session
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            width: 185
            color: Qt.rgba(0, 0, 0, 0.2)
            dropDownColor: Qt.rgba(0, 0, 0, 0.2)
            borderColor: "transparent"
            textColor: "white"
            arrowIcon: "images/arrow-down.png"
            arrowColor: "transparent"
            model: sessionModel
            index: sessionModel.lastIndex

            font.pixelSize: 16
            KeyNavigation.backtab: shutdown; KeyNavigation.tab: user_entry
        }
        
        Timer {
            id: timetr
            interval: 100
            running: true
            repeat: true
            onTriggered: {
                timelb.text = Qt.formatDateTime(new Date(), "HH:mm");
            }  
        }
        
        Rectangle {
            color: Qt.rgba(0, 0, 0, 0.2)
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            width: 60
            height: session.height
            
            Text {
                id: timelb
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                text: Qt.formatDateTime(new Date(), "HH:mm")
                color: "white"
                font.pixelSize: 18
            }
        }
        
        
    }
    
    Component.onCompleted: {
        /*if (user_entry.text === "")
            user_entry.focus = true
        else
            pw_entry.focus = true*/
        pw_entry.focus = true
    }
}

## Simplicity
Simple SDDM theme

![Screenshot](simplicity/simplicity-changed.png)


### Install
Move simplicity folder to 

    /usr/share/sddm/themes

edit file

    /etc/sddm.conf

search for

    [Theme]
    Current=theme-name

and change to 

    [Theme]
    Current=simplicity

#### ArchLinux
From [AUR](https://aur.archlinux.org/packages/simplicity-sddm-theme-git/)

#### Font
Recommended font: Noto Sans

If you use SDDM version 0.16 or above please read [this](https://github.com/sddm/sddm/wiki/0.16.0-Release-Announcement#configuration)

To change theme graphically and more use [SDDM Config Editor](https://github.com/hagabaka/sddm-config-editor)
